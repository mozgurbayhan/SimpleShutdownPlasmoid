# README #


### What is this repository for? ###

* This is a simple shutdown KDE plasmoid which raises shutdown popup when clicked.
* Ver. 1.0

### How do I get set up? ###

Installing : 

```
#!python


plasmapkg -i simpleshutdown.zip
```


### Licence ###
Copylefted by GPL v3

[http://www.gnu.org/copyleft/gpl.html](http://www.gnu.org/copyleft/gpl.html)

### Who am i ? ###

* Mehmet Özgür Bayhan
* mozgurbayhan@gmail.com
* [mozgurbayhan.com](mozgurbayhan.com)